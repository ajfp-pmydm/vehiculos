package PruebaVehículos;

import Vehículos.Coche;
import Vehículos.Moto;

public class VehículosTest {
	public static void main(String[] args) throws Exception {		
		Coche coche = new Coche();
		Moto moto = new Moto();
		moto.println();	
		coche.print();
		
		coche.rodar(40000);	
		coche.rodar(15000);		
		coche.reponerRueda("delantera izquierda");		
		
		System.out.println("\nEl motor ha sido utilizado durante " + coche.getMotor().getContador() + " km.");
		System.out.println("\nLa rueda delantera derecha ha recorrido " +
		coche.getRueda("delantera derecha").getRodaduraKm() + " km.");
		System.out.println("La rueda delantera izquierda ha recorrido " +
				coche.getRueda("delantera izquierda").getRodaduraKm() + " km.");		
		
		coche.getRueda("delantera derecha").pinchar();
		coche.rodar(1);
		coche.repararRuedas();	
		coche.rodar(1);
		
		moto.rodar(50000);
		moto.getRueda("trasera").pinchar();
		moto.rodar(100);
		moto.getRueda("trasera").reparar();
		moto.reponerRueda("trasera");
		moto.rodar(100);
			
		coche.rodar(800000);			
	}
}
