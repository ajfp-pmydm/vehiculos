package Vehículos;

public class Motor {
	private String marca;
	private int cilindrada;
	private double potencia;
	private final static int maxKm = 300000;

	private int contador = 0;
	
	public Motor(int cilindrada, double potencia, String marca) {
		this.cilindrada = cilindrada;	
		this.potencia = potencia;
		this.marca = marca;
	}
	
	public Motor(double potencia, int cilindrada, String marca) {
		this.cilindrada = cilindrada;	
		this.potencia = potencia;
		this.marca = marca;
	}
	
	//Sobrecargar constructores
	
	public Motor(){
		potencia = 110;
		cilindrada = 1600;
		marca = "Toyota";
	}	
	
	public void rodar(int km, String vehículo) throws Exception {
		if (contador + km > 300000) {
			throw new PinchazoException("\nNo es posible el desplazamiento. "
					+ "El motor del vehículo tipo " + vehículo.toUpperCase() + " ha "
					+ "alcanzado su límite de rodaje y debería cambiarse.");
		}
		System.out.println("\nEl motor del vehículo tipo " + vehículo.toUpperCase() + " funciona durante " + km + " km.");
		contador += km;
	}	
	public void print() {
		System.out.println(this.toString());
	}
	
	public void println() {
		System.out.print(this.toString());
	}	
	
	@Override
	public String toString() {
		return "El motor del vehículo es de la marca " + marca + " con potencia de " 
	    + potencia + "cv y cilindrada de " + cilindrada + "cc.";
	}	
	
	public String getMarca() {
		return marca;
	}

	public int getCilindrada() {
		return cilindrada;
	}

	public double getPotencia() {
		return potencia;
	}

	public static int getMaxkm() {
		return maxKm;
	}

	public int getContador() {
		return contador;
	}

}
