package Vehículos;

import java.util.ArrayList;
import java.util.List;

//Comentario

public class Coche extends Vehículo {
	private String marca;	
	private static final List<Rueda> ruedasCoche = new ArrayList<>(List.of(
			new Rueda().setLugar("delantera derecha"),
			new Rueda().setLugar("delantera izquierda"), 
			new Rueda().setLugar("trasera derecha"),
		    new Rueda().setLugar("trasera izquierda"), 
			new Rueda().setLugar("repuesto"))
			);	
	public Coche(Motor motor, String matricula, String marca) {
		this.matricula = matricula;
		this.motor = motor;		
		this.marca = marca;
	}

	public Coche() {
		matricula = "1234BCD";
		motor = new Motor();
		marca = "Volkswagen";
	}

	@Override
	public void rodar(int km)  {
		String tipo = this.getClass().getSimpleName();
		try {
			motor.rodar(km, tipo);
			for (Rueda rueda : ruedasCoche)	{
				if (rueda.getLugar() != "repuesto") {
					rueda.rodar(km, tipo);
				}
			}
		} catch (Exception e) {			
			System.out.println(e.getMessage());
		}		
	}

	@Override
	public void repararRuedas() {
		for (Rueda rueda : ruedasCoche)	{
			rueda.reparar();
		}	
	}

	@Override
	public void repararRueda(String posición) {
		for (Rueda rueda : ruedasCoche)	{
			if(rueda.getLugar() == posición) {
				rueda.reparar();
				break;
			}			
		}
	}	

	@Override
	public void print() {
		System.out.println("El vehículo es un coche de la marca " + marca + ".");
		System.out.print("La matrícula del vehículo es " + matricula + "\n" + motor.toString());
	}

	@Override
	public void println() {
		System.out.println("El vehículo es un coche de la marca " + marca + ".");
		System.out.println("La matrícula del vehículo es " + matricula + ".\n" + motor.toString() + "\n");
	}


	public String getMatrícula() {
		return matricula;
	}

	public Motor getMotor() {
		return motor;
	}

	public Rueda getRueda(String lugar) {
		return ruedasCoche.stream().filter(rueda -> rueda.getLugar().equals(lugar)).findFirst().get();
	}

	@Override
	public void reponerRueda(String posición) {	
		if (ruedasCoche.size() == 5) {
			for (Rueda rueda : ruedasCoche)	{
				if(rueda.getLugar() == posición) {					
					ruedasCoche.set(ruedasCoche.indexOf(rueda), ruedasCoche.remove(4).setLugar(posición));					
					break;
				}	
			}			

		} else {
			System.out.println("\nNo quedan ruedas de repuesto.");
		}

	}	

}
