package Vehículos;

public abstract class Vehículo {
	protected Motor motor;
	protected String matricula;
	public abstract void rodar(int kms) throws Exception;
	public abstract void print();
	public abstract void println();
	public abstract void repararRueda(String posición);
	public abstract void repararRuedas();
	public abstract void reponerRueda(String posición);
}
