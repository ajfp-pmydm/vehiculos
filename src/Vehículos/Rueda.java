package Vehículos;

public class Rueda {
	
	private String lugar;
	private String marca; 
	private int pulgadas; 
	private int anchuraMm; 
	private int ratio; 
	private static final int maxKm = 60000; 
	private int rodaduraKm = 0; 
	private boolean pinchada = false;		
	
	public Rueda(String marca, int pulgadas, int anchuraMm,int ratio) {
		this.marca = marca;
		this.pulgadas = pulgadas;
		this.anchuraMm = anchuraMm;
		this.ratio = ratio;		
	}
	
	Rueda(){
		marca = "Vossen";
		pulgadas = 16;
		anchuraMm = 205;
		ratio = 55;		
	}
	
	protected Object clone() throws CloneNotSupportedException{
		return this.clone();		
	}
	
	public void rodar(int km, String vehículo) throws Exception {
		if (rodaduraKm + km > 60000) {				
			throw new Exception("\nNo es posible el desplazamiento."
					+ "Una rueda del vehículo tipo " + vehículo.toUpperCase() 
					+ " ha alcanzado su límite de rodaje y debería cambiarse.");			
		}	
		if (pinchada) {
			throw new PinchazoException("\nLa rueda " + lugar +  " del vehículo tipo " 
		    + vehículo.toUpperCase() + " está pinchada y hay que repararla.");
		}
		System.out.println("\nLa rueda " + lugar + " del vehículo tipo " + vehículo.toUpperCase() + " recorre " + km + " km.");
		rodaduraKm += km;
	}
	
	public void pinchar() {
		pinchada = true;
	}
	
	public void reparar() {
		pinchada = false;
	}
	
	public boolean estáPinchada() {
		return pinchada;
	}	
	
	public Rueda setLugar(String lugar) {
		this.lugar = lugar;
		return this;
	}
	
	public String getLugar() {		
		return lugar;
	}
	
	public String getMarca() {
		return marca;
	}

	public int getPulgadas() {
		return pulgadas;
	}

	public int getAnchuraMm() {
		return anchuraMm;
	}

	public int getRatio() {
		return ratio;
	}

	public int getRodaduraKm() {
		return rodaduraKm;
	}

}
