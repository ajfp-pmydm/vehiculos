package Vehículos;

import java.util.ArrayList;
import java.util.List;

public class Moto extends Vehículo {
	private Motor motor;
	private final List<Rueda> ruedasMoto = new ArrayList<>(List.of( 
			new Rueda().setLugar("delantera"),
			new Rueda().setLugar("trasera"),
			new Rueda().setLugar("repuesto")));
	private String marca;

	public Moto() {
		matricula = "1234BCD";
		motor = new Motor();
		marca = "Kawasaki";
	}

	public Moto(String matricula, Motor motor, String marca) {
		this.matricula = matricula;
		this.motor = motor;	
		this.marca = marca;
	}

	@Override
	public void rodar(int km) {
		String tipo = this.getClass().getSimpleName();
		try {
			motor.rodar(km, tipo);
			ruedasMoto.get(0).rodar(km, tipo);
			ruedasMoto.get(1).rodar(km, tipo);
		} catch (Exception e) {			
			System.out.println(e.getMessage());
		}
		
	}

	@Override
	public void repararRueda(String posición) {
		if (posición.equals("delantera")) {
			ruedasMoto.get(0).reparar();
		} else {
			ruedasMoto.get(1).reparar();
		}		
	}

	@Override
	public void repararRuedas() {
		ruedasMoto.get(0).reparar();	
		ruedasMoto.get(1).reparar();		
	}

	@Override
	public void reponerRueda(String lugar) {
		try {
			if (lugar.equals("delantera")) {
				ruedasMoto.set(0, ruedasMoto.remove(2).setLugar("delantera"));
			} else {
				ruedasMoto.set(1, ruedasMoto.remove(2).setLugar("trasera"));
			}		
		} catch (IndexOutOfBoundsException e) {
          System.out.println("\n No quedan ruedas de repuesto");
		}
	}		


	@Override
	public void print() {
		System.out.println("El vehículo es una moto de la marca " + marca + ".");
		System.out.print("La matrícula del vehículo es " + matricula + "\n" + motor.toString());
	}

	@Override
	public void println() {
		System.out.println("El vehículo es una moto de la marca " + marca + ".");
		System.out.println("La matrícula del vehículo es " + matricula + ".\n" + motor.toString() + "\n");
	}
	
	public Rueda getRueda(String lugar) {
		return lugar == "delantera" ? ruedasMoto.get(0) : ruedasMoto.get(1);
	}

}
